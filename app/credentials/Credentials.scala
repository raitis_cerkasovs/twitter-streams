package credentials

import play.api.Play._
import play.api.libs.oauth.{RequestToken, ConsumerKey}

/**
 * Created by raitis on 27/08/15.
 */
object Credentials {

  def credentials: Option[(ConsumerKey, RequestToken)] =
    for {
      apiKey <- current.configuration.getString("twitter.apiKey")
      apiSecret <- current.configuration.getString("twitter.apiSecret")
      token <- current.configuration.getString("twitter.token")
      tokenSecret <- current.configuration.getString("twitter.tokenSecret")
    } yield (
      ConsumerKey(apiKey, apiSecret),
      RequestToken(token, tokenSecret)
      )

}
