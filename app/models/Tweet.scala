package models

import play.api.libs.json.JsPath
import play.api.libs.functional.syntax._


/**
 * Created by raitis on 28/08/15.
 */
// case class
case class Tweet (from: String, text: String)

// Json deserialization
object Tweet {
  implicit val tweetReads = (
    (JsPath \ "created_at").read[String] and (JsPath \ "text").read[String]
    )(Tweet.apply _)
}