package controllers

import scala.concurrent.Future
import javax.inject.Inject

import credentials._
import play.api.Play.current
import play.api.libs.iteratee.{Enumerator, Iteratee}
import play.api.libs.oauth._
import play.api.libs.ws.{WS, WSClient}
import play.api.mvc.{Action, Controller}

import scala.concurrent.ExecutionContext.Implicits.global


/**
 * Created by raitis on 01/09/15.
 */
class ITERATEEtests  @Inject() (ws: WSClient) extends Controller {

  /**
   * Enumerator and Iterator
   */
  val intEnumenator = Enumerator(1,3,6,2,2,9,3)
  val summingIteratee = Iteratee.fold(0) {
    (sum: Int, chunk: Int) => sum + chunk
  }
  val newIterateeFuture: Future[Iteratee[Int, Int]] = intEnumenator(summingIteratee)
  val resultFuture: Future[Int] = newIterateeFuture.flatMap(_.run)

  def en = Action {
    resultFuture.onComplete(sum => println(s"The sum is $sum"))
    Ok
  }




  /**
   * iterate Twitter stream
   */
  val loggingIterratee = Iteratee.foreach[Array[Byte]] {chunk =>
    println(new String(chunk, "UTF-8"))
  }

  def getTweets = Action {

    Credentials.credentials.map { case (consumerKey, requestToken) => {
      WS
        .url("https://stream.twitter.com/1.1/statuses/filter.json")
        // can be seen as a timeout for the stream
        .withRequestTimeout(10000)
        .sign(OAuthCalculator(consumerKey, requestToken))
        .withQueryString("track" -> "london")
        .get( _ => loggingIterratee)
       }
    } getOrElse println ("Error")
      Ok
  }



}