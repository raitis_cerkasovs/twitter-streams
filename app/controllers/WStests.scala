package controllers

import javax.inject.Inject

import credentials.Credentials
import models.Tweet
import play.api.Play.current
import play.api.libs.json.Json
import play.api.libs.oauth.OAuthCalculator
import play.api.libs.ws.{WS, WSClient}
import play.api.mvc._
import play.twirl.api.Html

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
 * Created by raitis on 26/08/15.
 */
class WStests @Inject() (ws: WSClient) extends Controller {

  def raitis= Action.async {
      WS
        .url("http://raitis.co.uk")
        .get()
        .map { response => println(response.body)
        Ok
      }
  }



  def inbox= Action.async {
    WS
      .url("https://login.inbox.lv/login/login")
      .post(Map("imapuser"->Seq("66x"), "pass"->Seq("")))
      .map { response => Ok(Html.apply(response.body))
    }
  }




  def search = Action.async {

    val results = 5
    val query = """London"""

    Credentials.credentials.map { case (consumerKey, requestToken) =>
      WS
        .url("https://api.twitter.com/1.1/search/tweets.json")
        .sign(OAuthCalculator(consumerKey, requestToken))
        .withQueryString("q" -> query, "count" -> results.toString())
        .get()
        .map { response =>
        val tweets = Json.parse(response.body).\("statuses").as[Seq[Tweet]]
        Ok(views.html.WStests.search(tweets))
      }
      // lets do asynchronosly in one go
    } getOrElse {
      // must be set as Future if Action.async
      Future {
        Ok
      }
    }
  }


}