package controllers

import javax.inject.Inject

import actors.WEBSOCKETStests.{Join, ChatRoom}
import akka.util.Timeout
import play.api.libs.iteratee.{Enumerator, Iteratee}
import play.api.libs.ws.WSClient
import play.api.mvc._
import play.api.libs.concurrent.{Akka, Promise}
import scala.concurrent.Future
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global
import play.api.Play.current

import akka.actor._
import akka.pattern.ask

/**
 * Created by raitis on 02/09/15.
 */
class WEBSOCKETStests @Inject() (ws: WSClient) extends Controller {


  /**
   * Load Random number with stream
   */
  def r = new scala.util.Random

  // generate random Int and send via WebSocket
  def loadAverage = WebSocket.using[String] { implicit request =>
     val in = Iteratee.ignore[String]
     val out = Enumerator.repeatM {
         Promise.timeout(r.nextInt(100).toString, 3 seconds)
        }
     (in, out)
  }

  def startLoadAverage = Action {
    Ok(views.html.WEBSOCKETStests.startLoadAverage())
  }


  /**
   * Chat Room
   */
  implicit val timeout = Timeout(1 seconds)
  val room = Akka.system.actorOf(Props[ChatRoom])


  def chatRoom(nick: String) = Action { implicit request =>
    Ok(views.html.WEBSOCKETStests.chatRoom(nick))
  }


  def chatSocket(nick: String) = WebSocket.tryAccept[String] { request =>
    // ? method is returning type Future.
    val channelsFuture = room ? Join(nick)
    // WebSocket cannot be .async, so you have to use tryAccept with Try block
    try {
      // actor always returning tuple (iterator, enumerator)
      channelsFuture.mapTo[(Iteratee[String, _], Enumerator[String])].map(e => Right(e))
    } catch { case e: Exception =>
      Future{Left(Ok("Error"))}
    }
  }

}





