package controllers

import actors.REACTIVE.TwitterStreamer
import credentials._
import play.Logger
import play.api.Play.current
import play.api.libs.concurrent.Execution.Implicits._
import play.api.libs.iteratee.{Concurrent, Enumeratee, Enumerator, Iteratee}
import play.api.libs.json.JsValue
import play.api.libs.oauth._
import play.api.libs.ws.WS
import play.api.mvc._
import play.extras.iteratees._


class REACTIVE extends Controller {






  def index = Action { implicit request =>
    Ok(views.html.REACTIVE.index(request))
  }






  def tweetsSocket = WebSocket.acceptWithActor[String, JsValue] {
    request => out => TwitterStreamer.props(out)
  }












  // if Action.async you dont have produce output view.
  def parseTweets = Action{

    val (iteratee, enumerator) = Concurrent.joined[Array[Byte]]
    // change to JsValue
    val jsonStream: Enumerator[JsValue] =
      enumerator &>
      Encoding.decode() &>
      Enumeratee.grouped(JsonIteratees.jsSimpleObject)


    val loggingIteratee = Iteratee.foreach[JsValue] { value =>
      Logger.info(value.toString())
    }

    jsonStream run loggingIteratee

    Credentials.credentials.map { case (consumerKey, requestToken) =>
        WS
          .url("https://stream.twitter.com/1.1/statuses/filter.json")
          .withRequestTimeout(10000)
          .sign(OAuthCalculator(consumerKey, requestToken))
          .withQueryString("track" -> "london")
          .get { response => Logger.info("Status: " + response.status)
              iteratee
           }.map { _ =>
           Ok("Stream closed")
         }
      }

    Ok
  }






}
