package actors.REACTIVE

import akka.actor.{Actor, ActorRef, Props}
import credentials.Credentials
import play.api.Logger
import play.api.libs.iteratee.{Iteratee, Enumeratee, Concurrent, Enumerator}
import play.api.libs.json.{JsObject}
import play.api.libs.oauth.OAuthCalculator
import play.api.libs.ws.WS
import play.extras.iteratees.{JsonIteratees, Encoding}
import scala.concurrent.ExecutionContext.Implicits.global
import play.api.Play.current


/**
 * Created by raitis on 25/08/15.
 */
class TwitterStreamer(out: ActorRef) extends Actor {

  def receive = {
    // tickle actor
    case "subscribe" => Logger.info("Recieved subscription from a client.")
    //  out ! Json.obj("text" -> "Hello Darling.") // can return basic stuff to page
    TwitterStreamer.subscribe(out)
  }

}





object TwitterStreamer {

  // companion object - only thing needed tickle actor from Controller
  def props(out: ActorRef) = Props(new TwitterStreamer(out))


  // define broadcaster
  private var broadcastEnumerator: Option[Enumerator[JsObject]] = None



  def connect():Unit = {

    // begin with credentials
    Credentials.credentials.map { case (consumerKey, requestToken) =>

      // define iteratee and enumerator
      val (iteratee, enumerator) = Concurrent.joined[Array[Byte]]

      // define pipe
      val jsonStream: Enumerator[JsObject] = enumerator &>
        Encoding.decode() &>
        Enumeratee.grouped(JsonIteratees.jsSimpleObject)

      // pass Bradcaster a streem
      val (be, _) = Concurrent.broadcast(jsonStream)
      broadcastEnumerator = Some(be)

      // connect
      WS
        .url("https://stream.twitter.com/1.1/statuses/filter.json")
        .withRequestTimeout(10000)
        .sign(OAuthCalculator(consumerKey, requestToken))
        .withQueryString("track" -> "London")
        .get { response => Logger.info("Status: " + response.status)
        iteratee
      }.map { _ =>
        Logger.info("Stream Closed.")
      }
    } getOrElse {
      Logger.info("Credentials Missing.")
    }

  }


  def subscribe(out: ActorRef) = {
    if (broadcastEnumerator == None) {
      connect()
    }
    // pass data to actor object return to the screen
    val twitterClient = Iteratee.foreach[JsObject] { t => out ! t}
    broadcastEnumerator.map { enumerator => enumerator run twitterClient }
  }


}