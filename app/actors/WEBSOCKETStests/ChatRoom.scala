package actors.WEBSOCKETStests

import akka.actor.Actor
import akka.routing.Broadcast
import play.api.libs.iteratee.{Enumerator, Iteratee, Concurrent}
import scala.concurrent.ExecutionContext.Implicits.global

/**
 * Created by raitis on 03/09/15.
 */

case class Join(nick: String)
case class Leave(nick: String)
case class Broadcast(nick: String)

class ChatRoom extends Actor {

  var users = Set[String]()
  val (enumerator, channel) = Concurrent.broadcast[String]

  def receive = {

    case Join(nick) => {
      if (!users.contains(nick)) {
        val iteratee = Iteratee.foreach[String] { message =>
          self ! Broadcast(s"$nick: $message")
        // map - next action after connect to wc. Which is Leave.
        } map { _ =>
          self ! Leave(nick)
        }

        users += nick

        channel.push(s"User $nick has joined, totaly ${users.size} users.")

        sender ! (iteratee, enumerator)

      } else {
        val enumerator = Enumerator(s"Nickname $nick is already in use.")
        // ignore all messages
        val iteratee = Iteratee.ignore
        sender ! (iteratee, enumerator)
      }
    }

    case Leave(nick) => {
      users -= nick
      channel.push(s"User $nick left the room, ${users.size} users left.")
    }

    case Broadcast(msg: String) => {
      channel.push(msg)
    }
  }

}
