
package views.html.REACTIVE

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object index_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

class index extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[RequestHeader,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(implicit request: RequestHeader):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.35*/("""

"""),_display_(/*3.2*/main/*3.6*/ {_display_(Seq[Any](format.raw/*3.8*/("""

    """),format.raw/*5.5*/("""<div id="tweets"></div>

    <script type="text/javascript">

        var url = """"),_display_(/*9.21*/routes/*9.27*/.REACTIVE.tweetsSocket().webSocketURL()),format.raw/*9.66*/("""";

        var tweetSocket = new WebSocket(url);

        tweetSocket.onmessage = function (events) """),format.raw/*13.51*/("""{"""),format.raw/*13.52*/("""
           """),format.raw/*14.12*/("""console.log(event);
           var data = JSON.parse(event.data);
           var tweet = document.createElement("p");
           var text = document.createTextNode(data.text)
           tweet.appendChild(text);
           document.getElementById("tweets").appendChild(tweet);
        """),format.raw/*20.9*/("""}"""),format.raw/*20.10*/(""";

        tweetSocket.onopen = function() """),format.raw/*22.41*/("""{"""),format.raw/*22.42*/("""
           """),format.raw/*23.12*/("""tweetSocket.send("subscribe");
        """),format.raw/*24.9*/("""}"""),format.raw/*24.10*/(""";

        // to make resailent.

        tweetSocket.onclose = function() """),format.raw/*28.42*/("""{"""),format.raw/*28.43*/("""
           """),format.raw/*29.12*/("""alert("Connection Lost.");
           // autoconnection..
        """),format.raw/*31.9*/("""}"""),format.raw/*31.10*/("""

    """),format.raw/*33.5*/("""</script>

""")))}),format.raw/*35.2*/("""
"""))
      }
    }
  }

  def render(request:RequestHeader): play.twirl.api.HtmlFormat.Appendable = apply(request)

  def f:((RequestHeader) => play.twirl.api.HtmlFormat.Appendable) = (request) => apply(request)

  def ref: this.type = this

}


}

/**/
object index extends index_Scope0.index
              /*
                  -- GENERATED --
                  DATE: Thu Sep 03 23:50:29 BST 2015
                  SOURCE: /Users/raitis/Dropbox/play/PlayWebAppWorkspace/REACTIVE/test-app-twitter-stream/app/views/REACTIVE/index.scala.html
                  HASH: 0132bcc180eeb841d5d67371e4afa56d6d610008
                  MATRIX: 543->1|671->34|699->37|710->41|748->43|780->49|888->131|902->137|961->176|1090->277|1119->278|1159->290|1470->574|1499->575|1570->618|1599->619|1639->631|1705->670|1734->671|1837->746|1866->747|1906->759|1999->825|2028->826|2061->832|2103->844
                  LINES: 20->1|25->1|27->3|27->3|27->3|29->5|33->9|33->9|33->9|37->13|37->13|38->14|44->20|44->20|46->22|46->22|47->23|48->24|48->24|52->28|52->28|53->29|55->31|55->31|57->33|59->35
                  -- GENERATED --
              */
          