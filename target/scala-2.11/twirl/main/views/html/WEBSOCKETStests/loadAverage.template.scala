
package views.html.WEBSOCKETStests

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object loadAverage_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

class loadAverage extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template0[play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply():play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](_display_(/*1.2*/main/*1.6*/ {_display_(Seq[Any](format.raw/*1.8*/("""

"""),format.raw/*3.1*/("""<script type="text/javascript">

    $(function() """),format.raw/*5.18*/("""{"""),format.raw/*5.19*/("""

      """),format.raw/*7.7*/("""war ws = new WebSocket(""""),_display_(/*7.32*/routes/*7.38*/.WEBSOCKETStests.loadAverage()),format.raw/*7.68*/("""")


      ws.onmessage = function(msg) """),format.raw/*10.36*/("""{"""),format.raw/*10.37*/(""" """),format.raw/*10.38*/("""$("#load-average").text(msg.data)"""),format.raw/*10.71*/("""}"""),format.raw/*10.72*/("""

    """),format.raw/*12.5*/("""}"""),format.raw/*12.6*/(""")

</script>

<h1>Avarage: <span id="load-average">x</span></h1>

Ok
""")))}))
      }
    }
  }

  def render(): play.twirl.api.HtmlFormat.Appendable = apply()

  def f:(() => play.twirl.api.HtmlFormat.Appendable) = () => apply()

  def ref: this.type = this

}


}

/**/
object loadAverage extends loadAverage_Scope0.loadAverage
              /*
                  -- GENERATED --
                  DATE: Wed Sep 02 22:18:09 BST 2015
                  SOURCE: /Users/raitis/Dropbox/play/PlayWebAppWorkspace/REACTIVE/test-app-twitter-stream/app/views/WEBSOCKETStests/startLoadAverage.scala.html
                  HASH: 79d149005a1df9cad1cc768611e96d2f84a0c67e
                  MATRIX: 637->1|648->5|686->7|714->9|791->59|819->60|853->68|904->93|918->99|968->129|1036->169|1065->170|1094->171|1155->204|1184->205|1217->211|1245->212
                  LINES: 25->1|25->1|25->1|27->3|29->5|29->5|31->7|31->7|31->7|31->7|34->10|34->10|34->10|34->10|34->10|36->12|36->12
                  -- GENERATED --
              */
          