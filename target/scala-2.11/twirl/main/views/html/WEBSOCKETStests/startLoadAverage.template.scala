
package views.html.WEBSOCKETStests

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object startLoadAverage_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

class startLoadAverage extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template0[play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply():play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](_display_(/*1.2*/main/*1.6*/ {_display_(Seq[Any](format.raw/*1.8*/("""

"""),format.raw/*3.1*/("""<script type="text/javascript">

    $(function() """),format.raw/*5.18*/("""{"""),format.raw/*5.19*/("""

      """),format.raw/*7.7*/("""var ws = new WebSocket("ws://localhost:9000"""),_display_(/*7.51*/routes/*7.57*/.WEBSOCKETStests.loadAverage()),format.raw/*7.87*/("""")


      ws.onmessage = function(msg) """),format.raw/*10.36*/("""{"""),format.raw/*10.37*/(""" """),format.raw/*10.38*/("""$("#load-average").text(msg.data) """),format.raw/*10.72*/("""}"""),format.raw/*10.73*/("""

    """),format.raw/*12.5*/("""}"""),format.raw/*12.6*/(""")

</script>

<h1>Avarage: <span id="load-average">x</span></h1>

""")))}))
      }
    }
  }

  def render(): play.twirl.api.HtmlFormat.Appendable = apply()

  def f:(() => play.twirl.api.HtmlFormat.Appendable) = () => apply()

  def ref: this.type = this

}


}

/**/
object startLoadAverage extends startLoadAverage_Scope0.startLoadAverage
              /*
                  -- GENERATED --
                  DATE: Thu Sep 03 13:04:00 BST 2015
                  SOURCE: /Users/raitis/Dropbox/play/PlayWebAppWorkspace/REACTIVE/test-app-twitter-stream/app/views/WEBSOCKETStests/startLoadAverage.scala.html
                  HASH: ff59cc03ffab0692a8c8bed22bbc2758c11bc7f1
                  MATRIX: 647->1|658->5|696->7|724->9|801->59|829->60|863->68|933->112|947->118|997->148|1065->188|1094->189|1123->190|1185->224|1214->225|1247->231|1275->232
                  LINES: 25->1|25->1|25->1|27->3|29->5|29->5|31->7|31->7|31->7|31->7|34->10|34->10|34->10|34->10|34->10|36->12|36->12
                  -- GENERATED --
              */
          