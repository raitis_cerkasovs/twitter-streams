
package views.html.WEBSOCKETStests

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object chatShowRoom_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

class chatShowRoom extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[String,RequestHeader,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(nick: String)(implicit request: RequestHeader):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.49*/("""

"""),_display_(/*3.2*/main/*3.6*/{_display_(Seq[Any](format.raw/*3.7*/("""
"""),format.raw/*4.1*/("""<h1>Chatroom - You are """),_display_(/*4.25*/nick),format.raw/*4.29*/("""</h1>

  <form id="chatroom">
      <input id="text" placeholder="Say something" />
      <button type="submit">Send</button>
  </form>


<script type="text/javascript">

$(function() """),format.raw/*14.14*/("""{"""),format.raw/*14.15*/("""

    """),format.raw/*16.5*/("""ws = new WebSocket("ws://localhost:9000/")


"""),format.raw/*19.1*/("""}"""),format.raw/*19.2*/(""")

</script>

""")))}))
      }
    }
  }

  def render(nick:String,request:RequestHeader): play.twirl.api.HtmlFormat.Appendable = apply(nick)(request)

  def f:((String) => (RequestHeader) => play.twirl.api.HtmlFormat.Appendable) = (nick) => (request) => apply(nick)(request)

  def ref: this.type = this

}


}

/**/
object chatShowRoom extends chatShowRoom_Scope0.chatShowRoom
              /*
                  -- GENERATED --
                  DATE: Thu Sep 03 00:03:45 BST 2015
                  SOURCE: /Users/raitis/Dropbox/play/PlayWebAppWorkspace/REACTIVE/test-app-twitter-stream/app/views/WEBSOCKETStests/chatRoom.scala.html
                  HASH: 3574f75677a34215087add12e67d16b35a79c712
                  MATRIX: 571->1|713->48|741->51|752->55|789->56|816->57|866->81|890->85|1102->269|1131->270|1164->276|1236->321|1264->322
                  LINES: 20->1|25->1|27->3|27->3|27->3|28->4|28->4|28->4|38->14|38->14|40->16|43->19|43->19
                  -- GENERATED --
              */
          