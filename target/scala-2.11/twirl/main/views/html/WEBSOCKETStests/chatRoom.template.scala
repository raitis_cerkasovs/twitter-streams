
package views.html.WEBSOCKETStests

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object chatRoom_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

class chatRoom extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[String,RequestHeader,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(nick: String)(implicit request: RequestHeader):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.49*/("""

"""),_display_(/*3.2*/main/*3.6*/{_display_(Seq[Any](format.raw/*3.7*/("""
"""),format.raw/*4.1*/("""<h1>Chatroom - You are """),_display_(/*4.25*/nick),format.raw/*4.29*/("""</h1>

<form id="chatform">
    <input id="text" placeholder="Say something" />
    <button type="submit">Send</button>
</form>

<ul id="messages"></ul>

<script type="text/javascript">

$(function() """),format.raw/*15.14*/("""{"""),format.raw/*15.15*/("""

    """),format.raw/*17.5*/("""var ws = new WebSocket(""""),_display_(/*17.30*/routes/*17.36*/.WEBSOCKETStests.chatSocket(nick).webSocketURL()),format.raw/*17.84*/("""")

    ws.onmessage = function(msg) """),format.raw/*19.34*/("""{"""),format.raw/*19.35*/(""" """),format.raw/*19.36*/("""$("<li />").text(msg.data).appendTo("#messages") """),format.raw/*19.85*/("""}"""),format.raw/*19.86*/("""

    """),format.raw/*21.5*/("""$("#chatform").submit(function() """),format.raw/*21.38*/("""{"""),format.raw/*21.39*/("""
       """),format.raw/*22.8*/("""ws.send($("#text").val())
       $("#text").val("").focus()
       return false;
    """),format.raw/*25.5*/("""}"""),format.raw/*25.6*/(""")

"""),format.raw/*27.1*/("""}"""),format.raw/*27.2*/(""")

</script>

""")))}))
      }
    }
  }

  def render(nick:String,request:RequestHeader): play.twirl.api.HtmlFormat.Appendable = apply(nick)(request)

  def f:((String) => (RequestHeader) => play.twirl.api.HtmlFormat.Appendable) = (nick) => (request) => apply(nick)(request)

  def ref: this.type = this

}


}

/**/
object chatRoom extends chatRoom_Scope0.chatRoom
              /*
                  -- GENERATED --
                  DATE: Thu Sep 03 14:43:46 BST 2015
                  SOURCE: /Users/raitis/Dropbox/play/PlayWebAppWorkspace/REACTIVE/test-app-twitter-stream/app/views/WEBSOCKETStests/chatRoom.scala.html
                  HASH: 05aa347eaa502b7d12607f9b1a3024878fee9bae
                  MATRIX: 563->1|705->48|733->51|744->55|781->56|808->57|858->81|882->85|1110->285|1139->286|1172->292|1224->317|1239->323|1308->371|1373->408|1402->409|1431->410|1508->459|1537->460|1570->466|1631->499|1660->500|1695->508|1807->593|1835->594|1865->597|1893->598
                  LINES: 20->1|25->1|27->3|27->3|27->3|28->4|28->4|28->4|39->15|39->15|41->17|41->17|41->17|41->17|43->19|43->19|43->19|43->19|43->19|45->21|45->21|45->21|46->22|49->25|49->25|51->27|51->27
                  -- GENERATED --
              */
          