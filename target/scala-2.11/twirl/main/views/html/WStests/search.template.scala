
package views.html.WStests

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object search_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

class search extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[Seq[Tweet],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(tweets: Seq[Tweet]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.22*/("""
"""),_display_(/*2.2*/main/*2.6*/ {_display_(Seq[Any](format.raw/*2.8*/("""

"""),_display_(/*4.2*/tweets/*4.8*/.map/*4.12*/ { tweet =>_display_(Seq[Any](format.raw/*4.23*/("""

   """),format.raw/*6.4*/("""<ul>
       <li><span>"""),_display_(/*7.19*/tweet/*7.24*/.from),format.raw/*7.29*/("""</span>: """),_display_(/*7.39*/tweet/*7.44*/.text),format.raw/*7.49*/("""</li>
   </ul>

   """)))}),format.raw/*10.5*/("""
""")))}))
      }
    }
  }

  def render(tweets:Seq[Tweet]): play.twirl.api.HtmlFormat.Appendable = apply(tweets)

  def f:((Seq[Tweet]) => play.twirl.api.HtmlFormat.Appendable) = (tweets) => apply(tweets)

  def ref: this.type = this

}


}

/**/
object search extends search_Scope0.search
              /*
                  -- GENERATED --
                  DATE: Fri Sep 04 00:08:02 BST 2015
                  SOURCE: /Users/raitis/Dropbox/play/PlayWebAppWorkspace/REACTIVE/test-app-twitter-stream/app/views/WStests/search.scala.html
                  HASH: c06fedce844bcff96608a8d836ef66e2a422a82e
                  MATRIX: 541->1|656->21|683->23|694->27|732->29|760->32|773->38|785->42|833->53|864->58|913->81|926->86|951->91|987->101|1000->106|1025->111|1075->131
                  LINES: 20->1|25->1|26->2|26->2|26->2|28->4|28->4|28->4|28->4|30->6|31->7|31->7|31->7|31->7|31->7|31->7|34->10
                  -- GENERATED --
              */
          