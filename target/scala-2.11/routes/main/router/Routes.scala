
// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/raitis/Dropbox/play/PlayWebAppWorkspace/REACTIVE/test-app-twitter-stream/conf/routes
// @DATE:Fri Sep 04 00:33:36 BST 2015

package router

import play.core.routing._
import play.core.routing.HandlerInvokerFactory._
import play.core.j._

import play.api.mvc._

import _root_.controllers.Assets.Asset

class Routes(
  override val errorHandler: play.api.http.HttpErrorHandler, 
  // @LINE:6
  REACTIVE_0: controllers.REACTIVE,
  // @LINE:14
  WStests_2: controllers.WStests,
  // @LINE:22
  ITERATEEtests_1: controllers.ITERATEEtests,
  // @LINE:28
  WEBSOCKETStests_4: controllers.WEBSOCKETStests,
  // @LINE:37
  Assets_3: controllers.Assets,
  val prefix: String
) extends GeneratedRouter {

   @javax.inject.Inject()
   def this(errorHandler: play.api.http.HttpErrorHandler,
    // @LINE:6
    REACTIVE_0: controllers.REACTIVE,
    // @LINE:14
    WStests_2: controllers.WStests,
    // @LINE:22
    ITERATEEtests_1: controllers.ITERATEEtests,
    // @LINE:28
    WEBSOCKETStests_4: controllers.WEBSOCKETStests,
    // @LINE:37
    Assets_3: controllers.Assets
  ) = this(errorHandler, REACTIVE_0, WStests_2, ITERATEEtests_1, WEBSOCKETStests_4, Assets_3, "/")

  import ReverseRouteContext.empty

  def withPrefix(prefix: String): Routes = {
    router.RoutesPrefix.setPrefix(prefix)
    new Routes(errorHandler, REACTIVE_0, WStests_2, ITERATEEtests_1, WEBSOCKETStests_4, Assets_3, prefix)
  }

  private[this] val defaultPrefix: String = {
    if (this.prefix.endsWith("/")) "" else "/"
  }

  def documentation = List(
    ("""GET""", this.prefix, """controllers.REACTIVE.index"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """tweetsSocket""", """controllers.REACTIVE.tweetsSocket()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """parseTweets""", """controllers.REACTIVE.parseTweets"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """search""", """controllers.WStests.search"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """raitis""", """controllers.WStests.raitis"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """inbox""", """controllers.WStests.inbox"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """getTweets""", """controllers.ITERATEEtests.getTweets"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """en""", """controllers.ITERATEEtests.en"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """chatRoom/$nick<[^/]+>""", """controllers.WEBSOCKETStests.chatRoom(nick:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """chatSocket/$nick<[^/]+>""", """controllers.WEBSOCKETStests.chatSocket(nick:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """startLoadAverage""", """controllers.WEBSOCKETStests.startLoadAverage"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """loadAverage""", """controllers.WEBSOCKETStests.loadAverage()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """assets/$file<.+>""", """controllers.Assets.versioned(path:String = "/public", file:Asset)"""),
    Nil
  ).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
    case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
    case l => s ++ l.asInstanceOf[List[(String,String,String)]]
  }}


  // @LINE:6
  private[this] lazy val controllers_REACTIVE_index0_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix)))
  )
  private[this] lazy val controllers_REACTIVE_index0_invoker = createInvoker(
    REACTIVE_0.index,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.REACTIVE",
      "index",
      Nil,
      "GET",
      """ 10s stream, pipe Jsons to Actor and output. Full reactive App.""",
      this.prefix + """"""
    )
  )

  // @LINE:8
  private[this] lazy val controllers_REACTIVE_tweetsSocket1_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("tweetsSocket")))
  )
  private[this] lazy val controllers_REACTIVE_tweetsSocket1_invoker = createInvoker(
    REACTIVE_0.tweetsSocket(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.REACTIVE",
      "tweetsSocket",
      Nil,
      "GET",
      """ socket""",
      this.prefix + """tweetsSocket"""
    )
  )

  // @LINE:10
  private[this] lazy val controllers_REACTIVE_parseTweets2_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("parseTweets")))
  )
  private[this] lazy val controllers_REACTIVE_parseTweets2_invoker = createInvoker(
    REACTIVE_0.parseTweets,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.REACTIVE",
      "parseTweets",
      Nil,
      "GET",
      """ 10s stream, pipe Jsons to console""",
      this.prefix + """parseTweets"""
    )
  )

  // @LINE:14
  private[this] lazy val controllers_WStests_search3_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("search")))
  )
  private[this] lazy val controllers_WStests_search3_invoker = createInvoker(
    WStests_2.search,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WStests",
      "search",
      Nil,
      "GET",
      """ Find 5 tweets and send them to browser as a Seq""",
      this.prefix + """search"""
    )
  )

  // @LINE:16
  private[this] lazy val controllers_WStests_raitis4_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("raitis")))
  )
  private[this] lazy val controllers_WStests_raitis4_invoker = createInvoker(
    WStests_2.raitis,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WStests",
      "raitis",
      Nil,
      "GET",
      """ Return my homepage as a HTML to console""",
      this.prefix + """raitis"""
    )
  )

  // @LINE:18
  private[this] lazy val controllers_WStests_inbox5_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("inbox")))
  )
  private[this] lazy val controllers_WStests_inbox5_invoker = createInvoker(
    WStests_2.inbox,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WStests",
      "inbox",
      Nil,
      "GET",
      """ Login to inbox.lv (need correct password)""",
      this.prefix + """inbox"""
    )
  )

  // @LINE:22
  private[this] lazy val controllers_ITERATEEtests_getTweets6_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("getTweets")))
  )
  private[this] lazy val controllers_ITERATEEtests_getTweets6_invoker = createInvoker(
    ITERATEEtests_1.getTweets,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ITERATEEtests",
      "getTweets",
      Nil,
      "GET",
      """ 10s pront out tweets to console.""",
      this.prefix + """getTweets"""
    )
  )

  // @LINE:24
  private[this] lazy val controllers_ITERATEEtests_en7_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("en")))
  )
  private[this] lazy val controllers_ITERATEEtests_en7_invoker = createInvoker(
    ITERATEEtests_1.en,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ITERATEEtests",
      "en",
      Nil,
      "GET",
      """ Iteratee folds enumerator. Good sample""",
      this.prefix + """en"""
    )
  )

  // @LINE:28
  private[this] lazy val controllers_WEBSOCKETStests_chatRoom8_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("chatRoom/"), DynamicPart("nick", """[^/]+""",true)))
  )
  private[this] lazy val controllers_WEBSOCKETStests_chatRoom8_invoker = createInvoker(
    WEBSOCKETStests_4.chatRoom(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WEBSOCKETStests",
      "chatRoom",
      Seq(classOf[String]),
      "GET",
      """ Bi directional Chat. Actor.""",
      this.prefix + """chatRoom/$nick<[^/]+>"""
    )
  )

  // @LINE:30
  private[this] lazy val controllers_WEBSOCKETStests_chatSocket9_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("chatSocket/"), DynamicPart("nick", """[^/]+""",true)))
  )
  private[this] lazy val controllers_WEBSOCKETStests_chatSocket9_invoker = createInvoker(
    WEBSOCKETStests_4.chatSocket(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WEBSOCKETStests",
      "chatSocket",
      Seq(classOf[String]),
      "GET",
      """socket""",
      this.prefix + """chatSocket/$nick<[^/]+>"""
    )
  )

  // @LINE:32
  private[this] lazy val controllers_WEBSOCKETStests_startLoadAverage10_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("startLoadAverage")))
  )
  private[this] lazy val controllers_WEBSOCKETStests_startLoadAverage10_invoker = createInvoker(
    WEBSOCKETStests_4.startLoadAverage,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WEBSOCKETStests",
      "startLoadAverage",
      Nil,
      "GET",
      """ Enumerator repeatM return random number.""",
      this.prefix + """startLoadAverage"""
    )
  )

  // @LINE:34
  private[this] lazy val controllers_WEBSOCKETStests_loadAverage11_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("loadAverage")))
  )
  private[this] lazy val controllers_WEBSOCKETStests_loadAverage11_invoker = createInvoker(
    WEBSOCKETStests_4.loadAverage(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WEBSOCKETStests",
      "loadAverage",
      Nil,
      "GET",
      """ Socket""",
      this.prefix + """loadAverage"""
    )
  )

  // @LINE:37
  private[this] lazy val controllers_Assets_versioned12_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("assets/"), DynamicPart("file", """.+""",false)))
  )
  private[this] lazy val controllers_Assets_versioned12_invoker = createInvoker(
    Assets_3.versioned(fakeValue[String], fakeValue[Asset]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Assets",
      "versioned",
      Seq(classOf[String], classOf[Asset]),
      "GET",
      """ Map static resources from the /public folder to the /assets URL path""",
      this.prefix + """assets/$file<.+>"""
    )
  )


  def routes: PartialFunction[RequestHeader, Handler] = {
  
    // @LINE:6
    case controllers_REACTIVE_index0_route(params) =>
      call { 
        controllers_REACTIVE_index0_invoker.call(REACTIVE_0.index)
      }
  
    // @LINE:8
    case controllers_REACTIVE_tweetsSocket1_route(params) =>
      call { 
        controllers_REACTIVE_tweetsSocket1_invoker.call(REACTIVE_0.tweetsSocket())
      }
  
    // @LINE:10
    case controllers_REACTIVE_parseTweets2_route(params) =>
      call { 
        controllers_REACTIVE_parseTweets2_invoker.call(REACTIVE_0.parseTweets)
      }
  
    // @LINE:14
    case controllers_WStests_search3_route(params) =>
      call { 
        controllers_WStests_search3_invoker.call(WStests_2.search)
      }
  
    // @LINE:16
    case controllers_WStests_raitis4_route(params) =>
      call { 
        controllers_WStests_raitis4_invoker.call(WStests_2.raitis)
      }
  
    // @LINE:18
    case controllers_WStests_inbox5_route(params) =>
      call { 
        controllers_WStests_inbox5_invoker.call(WStests_2.inbox)
      }
  
    // @LINE:22
    case controllers_ITERATEEtests_getTweets6_route(params) =>
      call { 
        controllers_ITERATEEtests_getTweets6_invoker.call(ITERATEEtests_1.getTweets)
      }
  
    // @LINE:24
    case controllers_ITERATEEtests_en7_route(params) =>
      call { 
        controllers_ITERATEEtests_en7_invoker.call(ITERATEEtests_1.en)
      }
  
    // @LINE:28
    case controllers_WEBSOCKETStests_chatRoom8_route(params) =>
      call(params.fromPath[String]("nick", None)) { (nick) =>
        controllers_WEBSOCKETStests_chatRoom8_invoker.call(WEBSOCKETStests_4.chatRoom(nick))
      }
  
    // @LINE:30
    case controllers_WEBSOCKETStests_chatSocket9_route(params) =>
      call(params.fromPath[String]("nick", None)) { (nick) =>
        controllers_WEBSOCKETStests_chatSocket9_invoker.call(WEBSOCKETStests_4.chatSocket(nick))
      }
  
    // @LINE:32
    case controllers_WEBSOCKETStests_startLoadAverage10_route(params) =>
      call { 
        controllers_WEBSOCKETStests_startLoadAverage10_invoker.call(WEBSOCKETStests_4.startLoadAverage)
      }
  
    // @LINE:34
    case controllers_WEBSOCKETStests_loadAverage11_route(params) =>
      call { 
        controllers_WEBSOCKETStests_loadAverage11_invoker.call(WEBSOCKETStests_4.loadAverage())
      }
  
    // @LINE:37
    case controllers_Assets_versioned12_route(params) =>
      call(Param[String]("path", Right("/public")), params.fromPath[Asset]("file", None)) { (path, file) =>
        controllers_Assets_versioned12_invoker.call(Assets_3.versioned(path, file))
      }
  }
}