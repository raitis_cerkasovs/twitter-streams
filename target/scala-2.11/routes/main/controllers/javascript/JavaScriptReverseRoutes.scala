
// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/raitis/Dropbox/play/PlayWebAppWorkspace/REACTIVE/test-app-twitter-stream/conf/routes
// @DATE:Fri Sep 04 00:33:36 BST 2015

import play.api.routing.JavaScriptReverseRoute
import play.api.mvc.{ QueryStringBindable, PathBindable, Call, JavascriptLiteral }
import play.core.routing.{ HandlerDef, ReverseRouteContext, queryString, dynamicString }


import _root_.controllers.Assets.Asset

// @LINE:6
package controllers.javascript {
  import ReverseRouteContext.empty

  // @LINE:37
  class ReverseAssets(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:37
    def versioned: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Assets.versioned",
      """
        function(file) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "assets/" + (""" + implicitly[PathBindable[Asset]].javascriptUnbind + """)("file", file)})
        }
      """
    )
  
  }

  // @LINE:6
  class ReverseREACTIVE(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:8
    def tweetsSocket: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.REACTIVE.tweetsSocket",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "tweetsSocket"})
        }
      """
    )
  
    // @LINE:6
    def index: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.REACTIVE.index",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + """"})
        }
      """
    )
  
    // @LINE:10
    def parseTweets: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.REACTIVE.parseTweets",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "parseTweets"})
        }
      """
    )
  
  }

  // @LINE:22
  class ReverseITERATEEtests(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:22
    def getTweets: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ITERATEEtests.getTweets",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "getTweets"})
        }
      """
    )
  
    // @LINE:24
    def en: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ITERATEEtests.en",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "en"})
        }
      """
    )
  
  }

  // @LINE:28
  class ReverseWEBSOCKETStests(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:34
    def loadAverage: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.WEBSOCKETStests.loadAverage",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "loadAverage"})
        }
      """
    )
  
    // @LINE:28
    def chatRoom: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.WEBSOCKETStests.chatRoom",
      """
        function(nick) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "chatRoom/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("nick", encodeURIComponent(nick))})
        }
      """
    )
  
    // @LINE:32
    def startLoadAverage: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.WEBSOCKETStests.startLoadAverage",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "startLoadAverage"})
        }
      """
    )
  
    // @LINE:30
    def chatSocket: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.WEBSOCKETStests.chatSocket",
      """
        function(nick) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "chatSocket/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("nick", encodeURIComponent(nick))})
        }
      """
    )
  
  }

  // @LINE:14
  class ReverseWStests(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:18
    def inbox: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.WStests.inbox",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "inbox"})
        }
      """
    )
  
    // @LINE:16
    def raitis: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.WStests.raitis",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "raitis"})
        }
      """
    )
  
    // @LINE:14
    def search: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.WStests.search",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "search"})
        }
      """
    )
  
  }


}