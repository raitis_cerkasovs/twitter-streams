
// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/raitis/Dropbox/play/PlayWebAppWorkspace/REACTIVE/test-app-twitter-stream/conf/routes
// @DATE:Fri Sep 04 00:33:36 BST 2015

package controllers;

import router.RoutesPrefix;

public class routes {
  
  public static final controllers.ReverseAssets Assets = new controllers.ReverseAssets(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseREACTIVE REACTIVE = new controllers.ReverseREACTIVE(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseITERATEEtests ITERATEEtests = new controllers.ReverseITERATEEtests(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseWEBSOCKETStests WEBSOCKETStests = new controllers.ReverseWEBSOCKETStests(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseWStests WStests = new controllers.ReverseWStests(RoutesPrefix.byNamePrefix());

  public static class javascript {
    
    public static final controllers.javascript.ReverseAssets Assets = new controllers.javascript.ReverseAssets(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseREACTIVE REACTIVE = new controllers.javascript.ReverseREACTIVE(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseITERATEEtests ITERATEEtests = new controllers.javascript.ReverseITERATEEtests(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseWEBSOCKETStests WEBSOCKETStests = new controllers.javascript.ReverseWEBSOCKETStests(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseWStests WStests = new controllers.javascript.ReverseWStests(RoutesPrefix.byNamePrefix());
  }

}
